#!/usr/bin/env python3

# MOOH Multi-ROM packer
# 2022 Tormod Volden

# Takes as argument the DECB binary of the menu runtime plus a list
# of ROM images (or DECB files) to include.
# Generates bootfile.bin and romimages.bin

import sys
import struct
from pathlib import Path

def digest_rom(filename):
    p = Path(filename)
    if not p.is_file():
        print(filename, 'is not a regular file')
        sys.exit(1)
    name = p.stem
    with p.open() as f:
       rom = p.read_bytes()
    size = p.stat().st_size # or len(rom)
    return (name, size, rom)

# This can be unlucky if a DECB has one of these sizes
def type_by_size(size):
    if size == 4096:
        return 4
    elif size == 8192:
        return 8
    elif size == 16384:
        return 16
    elif size == 32768:
        return 32
    else:
        return -1   # assume DECB

# Each ROM image must start on a sector boundary
def sector_padded(rom, size):
    toadd = 511 - (size - 1) % 512
    rom += b'\xff' * toadd
    size += toadd
    return rom, size

# Split out trailer from menu runtime DECB
# end_of_code only needed if table should follow tightly
def digest_code(codefile):
    p = Path(codefile)
    code = p.read_bytes()
    header = code[0:5]
    trailer = code[-5:]
    code = code [0:-5]
    if header[0] != 0:
        print(codefile, 'is not a DECB binary')
    header_len = struct.unpack('>h', header[1:3])[0]
    header_bas = struct.unpack('>h', header[3:5])[0]
    code_len = len(code) - 5
    # multisegment would need more work to extract base address
    # of last segment (to get last address used)
    if code_len != header_len:
        print(codefile, 'has more than one DECB segment?')
    end_of_code = header_bas + code_len
    return (end_of_code, code, trailer)

# The final binary will have:
# - code file (minus original DECB trailer)
# - DECB header for data segment
# - 2-byte pointer to namelist (end of ROM table)
# - ROM table
# - namelist (strings)
# - DECB trailer
# The packed ROM images are written to a separate file

sector_offset = 0x020000 # base sector for ROM images

# extract code and trailer from menu runtime DECB
codefile = sys.argv[1]
end_of_code, code, trailer = digest_code(codefile)
data_seg = 0x1c00 # would be end_of_code for tight packing

# from number of ROMs calculate ROM table size
num_roms = len(sys.argv) - 2
print('(info) number of entries', num_roms)

namelist_addr = data_seg + 2 + num_roms * 6
name_offset = namelist_addr
print('(info) end_of_code', end_of_code, 'namelist_addr', namelist_addr)

romtable = b''
namelist = b''

with open('romimages.bin', mode='wb') as romimage:
    for romfile in sys.argv[2:]:
        # allow @xx:dirname link to page number xx
        if romfile[0] == '@':
            page_no = int(romfile[1:3]) - 1
            name = romfile[4:]
            size = 0
            rom = b''
            romtype = -2
        else:
            name, size, rom = digest_rom(romfile)
            romtype = type_by_size(size)
            rom, size = sector_padded(rom, size)

        print(' name', name, 'size', size, 'type', romtype)
        if romtype == -1 and rom[0] != 0:
            print(name, 'is not a DECB and not a ROM')

        # 6-byte ROM table entry
        romtable += struct.pack('>h', name_offset)
        # (16-bit sector offset would span 512 * 64K, e.g. 2048 * 16K, 32MB)
        # absolute sector (24-bit, spans 8GB)
        if romtype == -2:
            romtable += struct.pack('>I', page_no)[1:4]
        else:
            romtable += struct.pack('>I', sector_offset)[1:4]
        romtable += struct.pack('b', romtype)

        # list of names
        bname = name.encode() + b'\x00'
        namelist += bname
        name_offset += len(bname)
        sector_offset += size // 512

        # write ROM to romimagefile
        romimage.write(rom)

# write code, namelist pointer, romtable, namelist to bootfile
segm_length = 2 + len(romtable) + name_offset - namelist_addr
segm_header = struct.pack('>bhh', 0, segm_length, data_seg)
namelist_pointer = struct.pack('>h', namelist_addr)
p = Path('bootfile.bin')
p.write_bytes(code + segm_header + namelist_pointer + romtable + namelist + trailer)

