; MOOH Multi-ROM run-time
; Display menu, handle keys, load and run ROM or DECB
; 2022 Tormod Volden

	IFDEF COCO
OUTCHR	equ	$A282	; print char A to DEVNUM
OUTSTR	equ	$B99C	; print string at X+1 to DEVNUM
POLCAT	equ	$A1CB	; reads keyboard, key in A
OUTNUM	equ	$BDCC	; print number in D to DEVNUM
	ELSE
OUTCHR	equ	$B54A	; print char A to DEVNUM
OUTSTR	equ	$90E5	; print string at X+1 to DEVNUM
POLCAT	equ	$8006	; reads keyboard, key in A
OUTNUM	equ	$957A	; print number in D to DEVNUM
	ENDC

; used by OUTSTR and OUTCHR
cursor		equ $0088

; from SDBDOS (SDBOOT ROM)
_blk_op_lba_v1	equ	$0602	; only SDBOOT v1
_blk_op_lba	equ	$0E09	; from SDBOOT v1.1

; multirom-packer.py will put these items here
namelp		equ $1c00	; points to end of ROM table, start of strings
romtable	equ namelp+2	; after namelist pointer
; FIXME since first item element is first name, namelist pointer is not needed

			; current SDBOOT code goes up to $14A0
	org	$1600	; SDBOOT buffer is at $19f0-$1bf5, kernels at $1c00
	bra	disppage

currpage	fdb 0	; current page number
pagstart	fdb 0	; where in ROM table does current page start
itemno		fcb 0	; which item on this page

; ROM table entry (6 bytes):
;	*name	; 2 bytes, strings stored after romtable
;	sector	; 3 bytes (spans 8GB of data)
;	type	; 1 byte: number of KB for 8/16/32KB ROM
;		; type -1 for DECB
;		; type -2 for "directory" entry, sector is a page number

cls
	ldx	#$6060
	ldu	#$0400
	stu	>cursor
clw	stx	,u++
	cmpu	#$0600
	blo	clw
	rts

; display menu page, page number stored in currpage
disppage
	jsr	cls
; print header with page number
	ldx	#pagetxt-1
	bsr	prtstr
	ldd	currpage
	addd	#1	; display "1" for first page (0)
	jsr	OUTNUM
	ldx	#$0420	; second line on screen
	stx	>cursor
; multiply currpage with 180 (30 items of 6 bytes)
	ldb	currpage+1
	lda	#180
	mul		; 180 times LSB
	std	pagstart
	ldb	currpage
	lda	#180
	mul		; 180 times MSB
	addb	pagstart
	stb 	pagstart
	ldd	pagstart
	addd	#romtable
	std	pagstart	; now start of page in table
	ldu	pagstart	; is tfr d,u cheaper?
	clra
peritem
; check if end of romtable is reached
	cmpu	namelp		; FIXME: or romtable, first item is first name
	bhs	donetbl
; print letter and space
	sta	itemno
	adda	#'A	; 0=A etc
	cmpa	#'Z
	bls	letr
	suba	#'Z+1	; special chars above Z: , . / ;
	ldx	#keytbl
	lda	a,x
letr	jsr	OUTCHR
	lda	#32
	jsr	OUTCHR
; print ROM name
	ldx	,u
	leax	-1,x
	bsr	prtstr
	ldd	>cursor
	subd	#1	; round up to %16 "tab" position
	andb	#~$0F
	addd	#16
	std	>cursor
	leau	6,u	; next entry
	lda	itemno
	inca
	cmpa	#30	; TODO also check cursor?
	blo	peritem
donetbl	bra	mloop

prtstr	pshs	u
	jsr	OUTSTR	; clobbers U
	puls	u,pc

; get key and act on it
mloop
	jsr	POLCAT
	beq	mloop
	cmpa	#'A
	blo	nletr
	cmpa	#'Z
	bhi	nletr
	suba	#'A
	bra	selitem
nletr	ldx	#keytbl	; special chars above Z
	clrb
maysp	tst	,x
	beq	maynum
	incb
	cmpa	,x+
	bne	maysp
	addb	#'Z-'A
	tfr	b,a
	bra	selitem
maynum	cmpa	#'0
	blo	mayoth
	cmpa	#'9
	bhi	mayoth
	suba	#'0
; interpret '1' as first page (index 0) and '0' as page 10
	bne	nten
	adda	#10
nten	deca
	sta	currpage+1
	clr	currpage
	lbra	disppage
mayoth
; TODO check for arrow keys
; shift + digit could select 10*page, would allow 100 pages * 30 ROMs
; or use ENTER -> digit for tens (one-finger operation)
	bra mloop

; item number in A
selitem
	pshs	a
	jsr	cls
	ldx	#loadtxt-1
	bsr 	prtstr
	puls	a
; display selected ROM name
	ldu	pagstart
	ldb	#6	; table entry size
	mul
	leau d,u
	ldx	,u
	leax	-1,x
	bsr 	prtstr
	ldx	#ellips-1
	bsr 	prtstr
; get sector and type
	lda	$0E00	; check for SDBOOT v1
	cmpa	#$12
	bne	bnv1
	ldx	#_blk_op_lba_v1
	bra	setlba
bnv1	ldx	#_blk_op_lba
setlba	clra
	ldb	2,u
	std	,x
	ldd	3,u
	std	2,x
	lda	5,u	; ROM type (size in KB or $FF = DECB)
	cmpa	#$FF
	beq	dodecb	; hook into sdboot
	cmpa	#$FE	; "directory" item, link to page
	bne	romsz
	ldx	3,u
	stx	currpage
	lbra 	disppage
; number of KB in A -> number of bytes in Y
romsz	lsla
	lsla
	clrb
	tfr	d,y	; number of bytes
	ldx	#$C000	; cartridge ROM location
	ldd	#$3F3F
	std	$FFA4	; map BASIC ROMs
; call "direct" (raw binary load) in SDBDOS
	; FIXME various flags etc
	lda	$0E00
	cmpa	#$12	; NOP
	bne	dnv1
	jsr	$1012	; only SDBDOS v1
	bra	gocart
dnv1	jsr	[$0E05]	; from SDBDOS v1.1

gocart	lda	#$40
	sta	$FF90	; enable MMU
	; TODO detect if DOS cartridge?
	jmp	$C000

dodecb
	; TODO how to get out of way to allow loading at $1C00
	; e.g. alternate loader running from another MMU page
	; TODO maybe set stack pointer
; jump to "loaddecb" in SDBDOS, will execute payload
	lda	$0E00
	cmpa	#$12	; NOP
	bne	lnv1
	jmp	$0F6D	; only SDBDOS v1
lnv1	jmp	[$0E03]	; from SDBDOS v1.1

keytbl	fcc	",./:"
	fcb	0
pagetxt	fcc	/MOOH MULTIROM V1        PAGE /
	fcb	0
loadtxt	fcc	/LOADING /
	fcb 	0
ellips	fcc	/ .../
	fcb	$0D,0

; end of this code, next segment should be at namelp:
; pointer to name list
; rom table
; name list

	end	disppage

; Development notes and ideas:
; - namelist could be smaller if zero-termination is replaced by checking
; location of next name (must then change name printing routine)
; - romtable could be smaller if using only 16-bit sector offset, but
; would then need hard-coded base offset (would allow 32MB of ROMs)
; - anyway with names of 8+1 chars and 6 byte rom table entries, there
; is between $1c00 and $7e00 room for 1600 rom entries...
; - instead of one huge flat table of all entries, have a table per page,
; and a linked list of page tables. Would save space if sparse pages,
; and with return links would be similar to directories in a file system...
